package fileController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class FileInitiator {

    private static FileHandler fileCreatorHandler;

    public FileInitiator() {
        fileCreatorHandler = FileHandler.getInstance();
    }


    public int getMin(int i) {
        return Math.min((i + 1) * fileCreatorHandler.taskPerThread, fileCreatorHandler.numberOfRecord);
    }

    public void initialPaymentFile() throws IOException {
        List<Runnable> runnables = new ArrayList<>();
        Files.write(FilePaths.paymentPath, "debtor\t1.10.100.1\t100000\n".getBytes());
        for (int i = 0; i < fileCreatorHandler.numberOfThread; i++) {
            int start = i * fileCreatorHandler.taskPerThread;
            runnables.add(new FileCreator("creditor\t1.20.100.", "\t100\n",
                    FilePaths.paymentPath, start,
                    getMin(i)));
        }

        fileCreatorHandler.runPool(runnables);
    }

    private void initialCash() throws IOException {
        List<Runnable> runnables = new ArrayList<>();
        Files.write(FilePaths.cashPath, "1.10.100.1\t100000\n".getBytes(), StandardOpenOption.APPEND);
        for (int i = 0; i < fileCreatorHandler.numberOfThread; i++) {
            int start = i * fileCreatorHandler.taskPerThread;
            runnables.add(new FileCreator("1.20.100.", "\t0\n",
                    FilePaths.cashPath, start, getMin(i)));
        }

        fileCreatorHandler.runPool(runnables);
    }

    public void contextInitialized() {
        try {
            initialPaymentFile();
            initialCash();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}

package fileController;

import DTOs.TransactionCashSyncDTO;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.Semaphore;

public class FileWriter implements Runnable {
    private List<TransactionCashSyncDTO> transactionCashSyncList;
    private Semaphore semaphore;

    public FileWriter(List<TransactionCashSyncDTO> transactionCashSyncList) {
        this.transactionCashSyncList = transactionCashSyncList;
        semaphore = new Semaphore(1);
    }

    @Override
    public void run() {
        for (TransactionCashSyncDTO item : transactionCashSyncList) {
            try {
                semaphore.acquire();
                Files.write(FilePaths.cashPath, item.getCashLog(), StandardOpenOption.APPEND);
                Files.write(FilePaths.transactionsPath, item.getTransactionLog(), StandardOpenOption.APPEND);
                semaphore.release();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


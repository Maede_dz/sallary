package fileController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.Semaphore;

public class FileCreator implements Runnable {
    private String content;
    private Path path;
    private String amount;
    private int end;
    private int start;
    private Semaphore semaphore;

    public FileCreator(String content, String amount, Path path, int start, int end) {
        this.content = content;
        this.path = path;
        this.amount = amount;
        this.end = end;
        this.start = start;
        semaphore = new Semaphore(1);
    }

    @Override
    public synchronized void run() {
        try {
            for (int i = start + 1; i <= end; i++) {
                semaphore.acquire();
                Files.write(path, (content + i + amount).getBytes(), StandardOpenOption.APPEND);
                semaphore.release();
            }
        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

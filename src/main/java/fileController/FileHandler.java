package fileController;

import DTOs.TransactionCashSyncDTO;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FileHandler {
    private static FileHandler instance;
    public final int numberOfThread;
    public final int numberOfLiveThread = 2;
    public final int numberOfRecord = 103;
    public final int taskPerThread = 10;

    public static FileHandler getInstance() {
        if (instance == null) {
            instance = new FileHandler();
        }
        return instance;
    }

    public FileHandler() {
        numberOfThread = numberOfRecord / taskPerThread + 1;
    }

    public void runPool(List<Runnable> runnables) {
        ExecutorService pool = Executors.newFixedThreadPool(numberOfLiveThread);
        for (Runnable runnable : runnables) {
            pool.execute(runnable);
        }
        pool.shutdown();
        try {
            pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            System.out.println("something is wrong in pool");
        }
    }


    public int getEndsOfListPerThread(int threadNumber) {
        return Math.min(threadNumber * taskPerThread,
                numberOfRecord + 1);
    }

    public void writeTransactionsCash(List<List<TransactionCashSyncDTO>> items, Path path) throws IOException {
        Files.write(FilePaths.transactionsPath, "".getBytes());
        List<Runnable> runnables = new ArrayList<>();
        for (int i = 0; i < FileHandler.getInstance().numberOfThread; i++) {
            runnables.add(new FileWriter(items.get(i)));
        }
        runPool(runnables);
    }


}

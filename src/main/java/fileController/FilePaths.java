package fileController;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FilePaths {

    private static String basePath = "/home/maede/Desktop/";

    public static Path paymentPath = Paths.get(basePath + "/payment.txt");
    public static Path cashPath = Paths.get(basePath + "/cash.txt");
    public static Path transactionsPath = Paths.get(basePath + "/transactions.txt");

}

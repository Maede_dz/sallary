package DTOs;

public class BankDTO {
    public String depositNumber;
    public boolean debtor;
    public int amount;

    public BankDTO(String[] info) {
        debtor = info[0].equals("debtor");
        depositNumber = info[1];
        amount = Integer.parseInt(info[2]);
    }
}

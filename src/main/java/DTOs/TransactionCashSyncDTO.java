package DTOs;

public class TransactionCashSyncDTO {
    private String debtorDepositNumber;
    private String creditorDepositNumber;
    public int amount;

    public TransactionCashSyncDTO(String debtorDepositNumber, String creditorDepositNumber, int amount) {
        this.debtorDepositNumber = debtorDepositNumber;
        this.creditorDepositNumber = creditorDepositNumber;
        this.amount = amount;
    }

    public byte[] getCashLog() {
        return (creditorDepositNumber + "\t" + amount + "\n").getBytes();
    }

    public byte[] getTransactionLog() {
        return (debtorDepositNumber + "\t" + creditorDepositNumber + "\t" + amount + "\n").getBytes();
    }
}

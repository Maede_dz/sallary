import DTOs.BankDTO;
import fileController.FilePaths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Payment {
    private static Payment instance;

    private final Path path = FilePaths.paymentPath;
    private List<BankDTO> banks;
    private BankDTO debtor;

    public static Payment getInstance() {
        if (instance == null) {
            instance = new Payment();
        }
        return instance;
    }

    public Payment() {
        banks = new ArrayList<>();
        debtor = null;
    }

    public List<BankDTO> getAllBankInfo() throws IOException {
        if (banks.size() != 0) {
            return banks;
        }
        banks = new ArrayList<>();
        Files.lines(path).forEach(line -> {
            String[] splitedline = line.split("\t");
            if (splitedline.length > 2) {
                banks.add(new BankDTO(line.split("\t")));
            }
        });
        return banks;
    }

    public int getAllCreditorAmountsSum() throws IOException {
        if (banks.size() == 0) {
            getAllBankInfo();
        }
        int amounts = 0;
        for (BankDTO b : banks) {
            if (!b.debtor) {
                amounts += b.amount;
            }
        }
        return amounts;
    }


    public BankDTO getDebtor() {
        if (debtor == null) {
            for (BankDTO b : banks) {
                if (b.debtor) {
                    debtor = b;
                }
            }
        }
        return debtor;
    }

    public boolean checkDebtorAmountChangeValidation(int value) {
        debtor = getDebtor();
        return debtor.amount >= value;
    }

    public void changeDebtorAmount(int value) {
        debtor = getDebtor();
        debtor.amount -= value;
    }
}

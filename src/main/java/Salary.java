import customExceptions.NotEnoughCashException;

import java.io.IOException;

public class Salary {

    public void payingSalary() {
        try {
            Payment paymentInstance = Payment.getInstance();
            int sum = paymentInstance.getAllCreditorAmountsSum();
            if (paymentInstance.checkDebtorAmountChangeValidation(sum)) {
                paymentInstance.changeDebtorAmount(sum);
                Cash.getInstance().updateCashFile();
            } else {
                throw new NotEnoughCashException("Debtor Cash isn't enough.");
            }

        } catch (IOException | NotEnoughCashException ex) {
            System.out.println(ex.getMessage());
        }

    }
}

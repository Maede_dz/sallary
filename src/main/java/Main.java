import fileController.FileInitiator;

public class Main {
    public static void main(String[] arg) {
        FileInitiator fileInitiator = new FileInitiator();
        fileInitiator.contextInitialized();
        Salary salary = new Salary();
        salary.payingSalary();
    }
}
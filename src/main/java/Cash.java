import DTOs.BankDTO;
import DTOs.TransactionCashSyncDTO;
import fileController.FileHandler;
import fileController.FilePaths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Cash {
    private static Cash instance;

    private final Path path = FilePaths.cashPath;


    public static Cash getInstance() {
        if (instance == null) {
            instance = new Cash();
        }
        return instance;
    }


    public void updateCashFile() throws IOException {
        List<List<TransactionCashSyncDTO>> items = new ArrayList<>();
        FileHandler fileCreatorHandlerIns = FileHandler.getInstance();
        Payment payment = Payment.getInstance();
        List<BankDTO> banks = payment.getAllBankInfo();
        BankDTO debtor = payment.getDebtor();
        Files.write(path, (debtor.depositNumber + "\t" + debtor.amount + "\n").getBytes());

        for (int i = 0; i < fileCreatorHandlerIns.numberOfThread; i++) {

            List<TransactionCashSyncDTO> perThread = new ArrayList<>();
            int start = i * fileCreatorHandlerIns.taskPerThread;

            for (int j = start; j < fileCreatorHandlerIns.getEndsOfListPerThread(i + 1); j++) {
                if (j == 0) {
                    continue;
                }
                BankDTO item = banks.get(j);
                perThread.add(new TransactionCashSyncDTO(debtor.depositNumber, item.depositNumber, item.amount));
            }
            items.add(perThread);
        }

        fileCreatorHandlerIns.writeTransactionsCash(items, path);
    }

}

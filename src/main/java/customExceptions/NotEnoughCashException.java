package customExceptions;

public class NotEnoughCashException extends Exception {
    public NotEnoughCashException(String msg) {
        super(msg);
    }
}
